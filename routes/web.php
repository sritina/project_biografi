<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route ::get('/index',[App\Http\Controller\indexcontroller :: class,'index']);
Route ::get('/about',[App\Http\Controller\aboutcontroller :: class,'about']);
Route ::get('/layout',[App\Http\Controller\layoutcontroller :: class,'layout']);




